let Artist = artifacts.require('./Artist.sol');

contract('Artist', (accounts) => {

    it('Application successfully deployed.', () => {
        let application = null;
        return Artist.deployed()
            .then((instance) => {
                application = instance;
                return application;
            }).then((app) => {
                assert.notEqual(app, null);
            });
    });

    it('Initially no artist exists.', () => {
        let application = null;
        return Artist.deployed()
            .then((instance) => {
                application = instance;
                return application.getArtistCount();
            }).then((count) => {
                assert.equal(count, 0);
            });
    });

    it('Can add an artist', () => {
        let application = null;
        return Artist.deployed()
            .then((instance) => {
                application = instance;
                return application.addArtistInformation(
                    1, true, 'Marshal Mathers', 
                    'Slim Shady', 'Cons'
                );
            }).then(() => {
                return application.getArtistCount();
            }).then((count) => {
                assert.equal(count, 1);
            });
    });

    it('Can get an artist', () => {
        let application = null;
        return Artist.deployed()
            .then((instance) => {
                application = instance;
                return application.addArtistInformation(
                    1, true, 'Marshal Mathers', 
                    'Slim Shady', 'Cons'
                );
            }).then(() => {
                return application.getArtistInformation(1);
            }).then((artist) => {
                assert.equal(parseInt(artist[0]), 1);
                assert.equal(artist[1], true);
                assert.equal(artist[2], 'Marshal Mathers');
                assert.equal(artist[3], 'Slim Shady');
                assert.equal(artist[4], 'Cons');
            });
    });

    it('Can update an artist', () => {
        let application = null;
        return Artist.deployed()
            .then((instance) => {
                application = instance;
                return application.addArtistInformation(
                    1, true, 'Marshal Mathers', 
                    'Slim Shady', 'Cons'
                );
            }).then(() => {
                return application.updateArtistInformation(
                    1, true, 'Marshal Mathers', 
                    'Slim Shady', 'Pro Cons'
                );
            }).then(() => {
                return application.getArtistInformation(1);
            }).then((artist) => {
                assert.equal(parseInt(artist[0]), 1);
                assert.equal(artist[1], true);
                assert.equal(artist[2], 'Marshal Mathers');
                assert.equal(artist[3], 'Slim Shady');
                assert.equal(artist[4], 'Pro Cons');
            });
    });


});
