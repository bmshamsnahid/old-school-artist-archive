let Message = artifacts.require('./Message.sol');

contract('Message', (accounts) => {

    it('Initialize with constructor values', () => {
        let app;
        return Message.deployed('Hello World')
            .then((instance) => {
                app = instance;
                return app.getMessage();
            }).then((message) => {
                assert.equal(message, 'Hello World');
            });
    });

    it('Can Update message', () => {
        let app;
        return Message.deployed('Hello World')
            .then((instance) => {
                app = instance;
                app.setMessage('Updated Message');
                return app.getMessage();
            }).then((message) => {
                assert.equal(message, 'Updated Message')
            })
    });
});
