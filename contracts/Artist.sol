pragma solidity ^0.5.0;

contract Artist {
    struct ArtistInformation  {
        uint id;
        bool isAlive;
        string actualName;
        string streetName;
        string lifeStyle;
    }

    mapping(uint => ArtistInformation) public artistInformationMap;
    uint public artistCount;

    function getArtistCount() public view returns (uint) {
        return artistCount;
    }

    function addArtistInformation(
        uint _id, 
        bool _isAlive, 
        string memory _actualName, 
        string memory _streetName, 
        string memory _lifeStyle) public payable returns (bool) {
        
        artistInformationMap[_id] = ArtistInformation(
            _id,
            _isAlive,
            _actualName,
            _streetName,
            _lifeStyle
        );
        artistCount ++;

        return true;
    }

    function getArtistInformation(uint _id) public view returns (
        uint, bool, string memory, string memory, string memory
    ) {
        return (
            artistInformationMap[_id].id,
            artistInformationMap[_id].isAlive,
            artistInformationMap[_id].actualName,
            artistInformationMap[_id].streetName,
            artistInformationMap[_id].lifeStyle
        );
    }

    function updateArtistInformation(
        uint _id, 
        bool _isAlive,
        string memory _actualName, 
        string memory _streetName, 
        string memory _lifeStyle
    ) public payable returns (bool) {
        artistInformationMap[_id].isAlive = _isAlive;
        artistInformationMap[_id].actualName = _actualName;
        artistInformationMap[_id].streetName = _streetName;
        artistInformationMap[_id].lifeStyle = _lifeStyle;
        
        return true;
    } 
}
