pragma solidity ^0.5.0;

contract Message {
    string public message;

    constructor(string memory _initialMessage) public {
        message = _initialMessage;
    }

    function setMessage(string memory newMessage) public {
        message = newMessage;
    }
    
    function getMessage() public view returns (string memory) {
        return message;
    }
}
