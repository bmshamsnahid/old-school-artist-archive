const Tx = require('ethereumjs-tx');
const Web3 = require('web3');

const config = require('./config');

const web3 = new Web3(config.infuraNode);

const account= {
    address: config.accountAddress,
    privateKey: Buffer.from(config.rawPrivateAccountKey, 'hex'),
};
    
const artistContract = require('../build/contracts/Artist');

// const abi = artistContract.abi;
const byteCode = artistContract.bytecode;

// Deploying the smart contract using data
web3.eth.getTransactionCount(account.address, (err, txCount) => {

    // create transaction
    const txObject = {
        nonce: web3.utils.toHex(txCount),
        gasLimit: web3.utils.toHex(4000000),
        gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
        data: byteCode,
    };

    // sign the transaction
    const tx = new Tx(txObject);
    tx.sign(account.privateKey);

    const serializeTx = tx.serialize();
    const raw = '0x' + serializeTx.toString('hex');

    //broadcast the transaction
    web3.eth.sendSignedTransaction(raw, (err, txHash) => {
        if(err) console.log(err);
        console.log(txHash);
    });
});
