const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const router = require('./route');

app.use(express.json());
app.use(bodyParser.json({ extended: false }));
app.use(morgan('tiny'));

app.use(express.static('public'));

app.use('/api/artist', router);

const port = process.env.port || 8080;

app.listen(port, () => {
    console.log(`App is running on port: ${port}`);
});
