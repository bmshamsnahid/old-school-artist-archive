const express = require('express');
const router = express.Router();
const {
    addArtistInformation,
    getArtistInformation,
    countArtistInformation,
    getAllArtistInformation,
    updateArtistInformation,
} = require('./service');

router.post('/', async (req, res, next) => {
    const id = req.body.id;
    const isAlive = req.body.isAlive;
    const actualName = req.body.actualName;
    const streetName = req.body.streetName;
    const lifeStyle = req.body.lifeStyle;
    
    const transactionHash = await addArtistInformation(
        id,
        isAlive,
        actualName,
        streetName,
        lifeStyle
    );
    
    res.status(200).json({
        'message': 'Creating an artist.',
        'txHash': transactionHash,
    });
});

router.get('/:id', async (req, res, next) => {
    const id = req.params.id;

    const artist = await getArtistInformation(id);

    res.status(200).json({
        'message': 'Getting individual artist.',
        'artist': artist,
    });
});

router.get('/', async (req, res, next) => {
    const artists = await getAllArtistInformation();
    res.status(200).json({
        'message': 'Geting all artist.',
        'artist': artists,
    });
});

router.put('/:id', async (req, res, next) => {
    const id = req.body.id;
    const isAlive = req.body.isAlive;
    const actualName = req.body.actualName;
    const streetName = req.body.streetName;
    const lifeStyle = req.body.lifeStyle;

    console.log(req.body);
    
    const transactionHash = await updateArtistInformation(
        id,
        isAlive,
        actualName,
        streetName,
        lifeStyle
    );

    res.status(200).json({
        'message': 'Updating an artist.',
        'txHash': transactionHash,
    });
});

module.exports = router;
