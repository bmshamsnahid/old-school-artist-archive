const config = require('./config')['local'];
const Tx = require('ethereumjs-tx');
const Web3 = require('web3')
const artistContract = require('../build/contracts/Artist');

const web3 = new Web3(config.infuraNode);

const account= {
    address: config.accountAddress,
    privateKey: Buffer.from(config.rawPrivateAccountKey, 'hex'),
};

const contractAddress = config.contractAddress;
const contractABI = artistContract.abi;
const byteCode = artistContract.bytecode;
const contract = new web3.eth.Contract(contractABI, contractAddress);

const getBalance = async (account) => {
    const balance = await web3.eth.getBalance(account.address);
    return {
        account: account,
        balance: balance
    };
};

const addArtistInformation = async (id, isAlive, actualName, streetName, lifeStyle) => {
    const data = contract.methods.addArtistInformation(id, isAlive, actualName, streetName, lifeStyle).encodeABI();
    const txCount = await web3.eth.getTransactionCount(account.address);
    // create transaction object
    const txObject = {
        nonce: web3.utils.toHex(txCount),
        gasLimit: web3.utils.toHex(800000),
        gasPrice: web3.utils.toHex(20* 1e9),
        to: contractAddress,
        data: data,
    };
    // sign the transaction
    const tx = new Tx(txObject);
    tx.sign(account.privateKey);

    const serializeTx = tx.serialize();
    const raw = '0x' + serializeTx.toString('hex');

    //broadcast the transaction
    const txHash = await web3.eth.sendSignedTransaction(raw);
    
    return txHash;
};

const getArtistInformation = async (id) => {
    const artistInformation = await contract.methods.getArtistInformation(id).call();
    const artist = {
        'id': artistInformation['0'],
        'isAlive': artistInformation['1'],
        'actualName': artistInformation['2'],
        'streetName': artistInformation['3'],
        'lifeStyle': artistInformation['4'],
    };

    return artist;
};

const countArtistInformation = async () => {
    const numberOfPerson = await contract.methods.getArtistCount().call();
    return numberOfPerson;
};

const getAllArtistInformation = async () => {
    const count = await countArtistInformation();
    let artists = [];
    for (let index=1; index<=count; index++) {
        const artistInformation = await getArtistInformation(index);
        artists.push(artistInformation);
    }
    return artists;
};

const updateArtistInformation = async (id, isAlive, actualName, streetName, lifeStyle) => {
    const data = contract.methods.updateArtistInformation(id, isAlive, actualName, streetName, lifeStyle).encodeABI();
    const txCount = await web3.eth.getTransactionCount(account.address);
    // create transaction object
    const txObject = {
        nonce: web3.utils.toHex(txCount),
        gasLimit: web3.utils.toHex(800000),
        gasPrice: web3.utils.toHex(20* 1e9),
        to: contractAddress,
        data: data,
    };
    // sign the transaction
    const tx = new Tx(txObject);
    tx.sign(account.privateKey);

    const serializeTx = tx.serialize();
    const raw = '0x' + serializeTx.toString('hex');

    //broadcast the transaction
    const txHash = await web3.eth.sendSignedTransaction(raw);
    
    return txHash;
};

const methodSimulator = async () => {
    // get accoutn balance
    // const result = await getBalance(account);
    // console.log(result);

    // write data to block chain
    // const transactionHash = await addArtistInformation(1, true, 'Jackson Jr', 'Ice Cube', 'Hustler');
    // const transactionHash = await addArtistInformation(2, false, 'Tupac Shakur', '2pac', 'Thug');
    // const transactionHash = await addArtistInformation(3, false, 'Eric Lynn Wright', 'Eazy M E', 'Gangsta Godfather');
    // console.log(transactionHash);

    // read individual data from blockchain
    // const artistInformation = await getArtistInformation(2);
    // console.log(artistInformation);

    // count the artists
    // const count = await countArtistInformation();
    // console.log(count);

    // read all the information from the blockchain
    // const allArtist = await getAllArtistInformation();
    // console.log(allArtist);

    // update an artist information
    // const transactionHash = await updateArtistInformation(2, false, 'Tupac Amaru Shakur', '2pac', 'Thug');
    // console.log(transactionHash);
};

// methodSimulator();

module.exports = {
    getBalance,
    addArtistInformation,
    getArtistInformation,
    countArtistInformation,
    getAllArtistInformation,
    updateArtistInformation,
}
